﻿/* This version of CommandSharp was designed for the Mosa Project. And is still subject to the MIT License. For more information please contact WinMister332 at contact@nerdhub.net or John Welsh at jwelsh@nerdhub.net
 * 
 * CommandSharp for Mosa
 * Written By WinMister332
 * Copyright (c) 2017-2021 NerdHub Technologies, All Rights Reserved.
 * ========================================
 * Class: CommandArguments.cs
 * Developer: WinMister332
 * Project: CommandSharp Mosa Edition
 * Date: Friday, September 3rd, 2021 @ 01:03 AM CST
 * License: MIT (https://opensource.org/licenses/MIT)
 *      You must include a copy of the MIT License with
 *      your code if you wish to use this API.
 * Repository: https://www.bitbucket.org/nerdhubtech/commandsharp_mosa/
 * Docs (For Full Edition Only): https://docs.nerdhub.net/CommandSharp/
 * ========================================
 */
using System.Collections.Generic;

namespace CommandSharp_Mosa
{
    public class CommandArguments
    {
        private List<string> arguments;

        public CommandArguments(string[] xargs)
        {
            arguments = new List<string>(xargs.Length);
            foreach (string s in xargs)
                arguments.Add(s);
        }

        public int Count => arguments.Count;

        public bool IsEmpty => Count < 1; //If less then 1, return true.

        /// <summary>
        /// Gets the argument at the specified position within the arguemnt array. (Note: Use argument safe position numbers.)
        /// </summary>
        /// <param name="pos">The position of the argument.</param>
        /// <returns>The argument.</returns>
        public string GetArgumentAtPosition(int pos)
            => (!IsEmpty) ? arguments[pos] : "";
    }
}