﻿/* This version of CommandSharp was designed for the Mosa Project. And is still subject to the MIT License. For more information please contact WinMister332 at contact@nerdhub.net
 * 
 * CommandSharp for Mosa
 * Written By WinMister332
 * Copyright (c) 2017-2021 NerdHub Technologies, All Rights Reserved.
 * ========================================
 * Class: CommandPrompt.cs
 * Developer: WinMister332
 * Project: CommandSharp Mosa Edition
 * Date: Friday, September 3rd, 2021 @ 01:03 AM CST
 * License: MIT (https://opensource.org/licenses/MIT)
 *      You must include a copy of the MIT License with
 *      your code if you wish to use this API.
 * Repository: https://www.bitbucket.org/nerdhubtech/commandsharp_mosa/
 * Docs (For Full Edition Only): https://docs.nerdhub.net/CommandSharp/
 * ========================================
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace CommandSharp_Mosa
{
    public class CommandPrompt
    {
        public CommandPrompt DEFAULT_INSTANCE = null;

        private CommandInvoker invoker;

        public bool DisplayEcho
        {
            get => GlobalSettings.DisplayEcho;
            set => GlobalSettings.DisplayEcho = value;
        }

        public string DisplayMessage
        {
            get => GlobalSettings.DisplayMessage;
            set => GlobalSettings.DisplayMessage = value;
        }

        public string MachineName
        {
            get => GlobalSettings.MachineName;
            set => GlobalSettings.MachineName = value;
        }

        public string CurrentUser
        {
            get => GlobalSettings.CurrentUser;
            set => GlobalSettings.CurrentUser = value;
        }

        //TODO: Add properties for Foreground and Background colors for the Console.

        public bool Debug
        {
            get => GlobalSettings.Debug;
            set => GlobalSettings.Debug = value;
        }

        public CommandPrompt(CommandInvoker invoker = null)
        {
            DEFAULT_INSTANCE = this;

            if (invoker == null)
                this.invoker = new CommandInvoker();
            else
                this.invoker = invoker;

            Debug = false;
            MachineName = "CommandSharp";
            CurrentUser = "Admin";

            //Set temporary message.
            DisplayMessage = "[$" + CurrentUser + "@" + MachineName + "]: > ";

            //TODO: Set the prompt in the created invoker class to 'this'.
        }
        
        //Exits the do while loop if set to true.
        private bool exitLoop = false;

        /// <summary>
        /// Shows a prompt to the user which allows them to input a command.
        /// </summary>
        /// <param name="loop">Constantly show the user the prompt?</param>
        public void Prompt(bool loop = true)
        {
            if (loop)
            {
                //Loop until 'exitLoop' is true.
                do
                {
                    Prompt();
                }
                while (!exitLoop); //While false.
            }
            else
                Prompt(); //Do once.
        }

        private void Prompt()
        {
            //Write the message to the console.
            if (DisplayEcho)
                Console.Write(DisplayMessage);
            //Prompt the user for input.
            var input = Console.ReadLine();
            //Check if null.
            if (input == "" || input == null || input == " ")
            {
                //Forward to invoker to be parsed.
                //invoker.Invoke(input);
            }
        }

        /// <summary>
        /// If true, the prompt stops looping and the code is terminated.
        /// </summary>
        public bool ExitLoop
        {
            get => exitLoop;
            set => exitLoop = value;
        }
    }
}
