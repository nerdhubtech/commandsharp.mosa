﻿/* This version of CommandSharp was designed for the Mosa Project. And is still subject to the MIT License. For more information please contact WinMister332 at contact@nerdhub.net
 * 
 * CommandSharp for Mosa
 * Written By WinMister332
 * Copyright (c) 2017-2021 NerdHub Technologies, All Rights Reserved.
 * ========================================
 * Class: CommandData.cs
 * Developer: WinMister332
 * Project: CommandSharp Mosa Edition
 * Date: Friday, September 3rd, 2021 @ 3:21PM CST
 * License: MIT (https://opensource.org/licenses/MIT)
 *      You must include a copy of the MIT License with
 *      your code if you wish to use this API.
 * Repository: https://www.bitbucket.org/nerdhubtech/commandsharp_mosa/
 * Docs (For Full Edition Only): https://docs.nerdhub.net/CommandSharp/
 * ========================================
 */
using System;

namespace CommandSharp_Mosa
{
    public class CommandData
    {
        private string name, author, description, syntax;
        private string[] aliases, dependencies, softDependencies;
        private bool hidden, allowHide;

        public CommandData(string name, string desc = "", string syntax = "", string author = "", string[] aliases = null, string[] dependencies = null, string[] softDependencies = null, bool hidden = false)
        {
            if (name == "" || name == "" || name == null)
                throw new NullReferenceException("The parameter \"name\" cannot be null or empty.");
            this.name = name;
            description = desc;
            this.syntax = syntax;
            this.author = author;
            this.aliases = (aliases == null) ? new string[0] : aliases;
            this.dependencies = (dependencies == null) ? new string[0] : dependencies;
            this.softDependencies = (softDependencies == null) ? new string[0] : softDependencies;

            //Check for hide prefixes.
            if (name.StartsWith("!") || name.StartsWith(".") || name.StartsWith("#") || name.StartsWith("@"))
            {
                hidden = true;
                allowHide = false;
                name = name.Remove(0, 1);
            }
            else
            {
                hidden = false;
                allowHide = true;
            }
        }

        public string GetName() => name;
        public string GetAuthor() => author;
        public string GetDescription() => description;
        public string GetSyntax() => syntax;

        public string[] GetAliases() => aliases;
        public string[] GetDependencies() => dependencies;
        public string[] GetSoftDependencies() => softDependencies;

        public bool IsHidden() => hidden;
        private bool CanHide() => allowHide;

        public bool Hidden
        {
            get => IsHidden();
            set
            {
                if (CanHide())
                    hidden = value;
            }
        }
    }
}