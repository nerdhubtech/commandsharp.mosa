﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandSharp_Mosa.Commands
{
    //Defines an example command that can used as a reference point to create a command.
    public class DummyCommand : Command
    {
        //This takes the place of the attribute of the same name. Since we can't use attributes, making a field and passing it is cleaner, but not the only option.
        private static readonly CommandData DATA = new CommandData("help", "Displays information about commands.", aliases: new string[] { "cmds", "?", "listcmds" });
        //The constructor is only needed to pass data about the command since we can't use an attribue.
        public DummyCommand() : base(DATA) {}

        //This is the entrypoint for the command, you put the code for what you want the command to do here when it's invoked.
        public override void OnInvoke(CommandProperties e)
        {
            //Check for an argument at position 0.
            if (e.Arguments.IsEmpty)
            {
                Console.WriteLine("No arguments were passed!");
            }
            else
            {
                string s = "";
                s = "Found " + e.Arguments.Count + " Arguments.";
                for (int i = 0; i < e.Arguments.Count; i++)
                {
                    s += "\n" + e.Arguments.GetArgumentAtPosition(i);
                }
                Console.WriteLine(s);
            }
        }
    }
}
