﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandSharp_Mosa.Commands
{
    public class HelpCommand : Command
    {
        //This takes the place of the attribute of the same name.
        private static readonly CommandData DATA = new CommandData("help", "Displays information about commands.", aliases: new string[] { "cmds", "?", "listcmds" });
        //The constructor is only needed to pass data about the command.
        public HelpCommand() : base(DATA) {}

        //This is the entrypoint for the command, you put the code for what you want the command to do here when it's invoked.
        public override void OnInvoke(CommandProperties e)
        {
            //Check if there's any arguments (We only want to support the -w argument for now.
            if (e.Arguments.IsEmpty)
            {
                //No arguments were passed.
                string s = "";
                foreach (Command c in e.Invoker.GetCommands())
                {
                    //If the command is not hidden, show it in help.
                    if (!c.IsHidden())
                    {
                        if (s.Length > 0)
                            s += "\n" + string.Format("{0}: {1}", c.GetName(), c.GetDescription());
                    }    
                }
                Console.WriteLine(s);
            }
            else
            {
                //One or more arguments were passed.
                //We only want the first one.
                var arg1 = e.Arguments.GetArgumentAtPosition(0);
                //For now, until CommandArgs is complete, you must manually check if the argument is a switch.
                if (arg1.Equals("-w"))
                {
                    //The wide switch is found.
                    string s = "";
                    int i = 0;
                    foreach (Command c in e.Invoker.GetCommands())
                    {
                        if (i == 5)
                        {
                            //Add a new line to the 's' then add the command.
                            s += "\n";
                            //Add the command to s.
                            s += c.GetName() + ", ";
                        }
                        else
                        {
                            // < 5.
                            s += c.GetName() + ", ";
                        }
                    }
                    //Return s.
                    Console.WriteLine(s);
                }
                else
                {
                    //Call the standard help command.
                    e.Invoker.ManualInvoke(this, new string[0], e);
                    return; //Exit the help command.
                }
            }
        }
    }
}
