﻿/* This version of CommandSharp was designed for the Mosa Project. And is still subject to the MIT License. For more information please contact WinMister332 at contact@nerdhub.net
 * 
 * CommandSharp for Mosa
 * Written By WinMister332
 * Copyright (c) 2017-2021 NerdHub Technologies, All Rights Reserved.
 * ========================================
 * Class: CommandProperties.cs
 * Developer: WinMister332
 * Project: CommandSharp Mosa Edition
 * Date: Friday, September 3rd, 2021 @ 01:03 AM CST
 * License: MIT (https://opensource.org/licenses/MIT)
 *      You must include a copy of the MIT License with
 *      your code if you wish to use this API.
 * Repository: https://www.bitbucket.org/nerdhubtech/commandsharp_mosa/
 * Docs (For Full Edition Only): https://docs.nerdhub.net/CommandSharp/
 * ========================================
 */
namespace CommandSharp_Mosa
{
    /// <summary>
    /// Defines the properties sugar the user can use when developing their command(s).
    /// </summary>
    public class CommandProperties
    {
        public CommandPrompt Prompt { get; protected set; }
        public CommandInvoker Invoker { get; protected set; }
        public CommandArguments Arguments { get; protected set; }
        public string PassedName { get; protected set; }

        public CommandProperties(CommandPrompt prompt, CommandInvoker invoker, CommandArguments arguments, string passedName = "")
        {
            Prompt = prompt;
            Invoker = invoker;
            Arguments = arguments;
            PassedName = passedName;
        }
    }
}