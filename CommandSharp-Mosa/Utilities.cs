﻿/* This version of CommandSharp was designed for the Mosa Project. And is still subject to the MIT License. For more information please contact WinMister332 at contact@nerdhub.net or John Welsh at jwelsh@nerdhub.net
 * 
 * CommandSharp for Mosa
 * Written By WinMister332
 * Copyright (c) 2017-2021 NerdHub Technologies, All Rights Reserved.
 * ========================================
 * Class: Utilities.cs
 * Developer: WinMister332
 * Project: CommandSharp Mosa Edition
 * Date: Friday, September 3rd, 2021 @ 01:03 AM CST
 * License: MIT (https://opensource.org/licenses/MIT)
 *      You must include a copy of the MIT License with
 *      your code if you wish to use this API.
 * Repository: https://www.bitbucket.org/nerdhubtech/commandsharp_mosa/
 * Docs (For Full Edition Only): https://docs.nerdhub.net/CommandSharp/
 * ========================================
 */
using System;
using System.Collections.Generic;

namespace CommandSharp_Mosa
{
    public static class Utilities
    {
        public static T[] Skip<T>(T[] objArray, int count)
        {
            List<T> sL = new List<T>();
            for (int i = 0; i < objArray.Length; i++)
            {
                if (i <= (count - 1))
                    continue;
                else
                    sL.Add(objArray[i]);
            }
            return sL.ToArray();
        }

        public static string[] Split(string str, string spl)
        {
            List<string> sL = new List<string>();
            string sx = "";
            for (int i = 0; i < str.Length; i++)
            {
                var c = str[i];
                //Do a constant test.
                if (sx.EndsWith(spl))
                {
                    //Manually remove the spl string, add the SX value to SL, then clear sx and continue parsing.
                    sx = sx.Remove(sx.Length - spl.Length, spl.Length);
                    sL.Add(sx);
                    sx = "";
                    //Add to SX.
                    sx += c;
                }
                else
                    sx += c;
            }
            //Check if the spl string exists at the start or end that was not processed and process it.
            if (!(string.IsNullOrEmpty(sx) && string.IsNullOrWhiteSpace(sx)))
            {
                if (sx.EndsWith(spl))
                    sx = sx.Remove(sx.Length - spl.Length, spl.Length);
                else if (sx.StartsWith(spl))
                    sx = sx.Remove(0, spl.Length);
                sL.Add(sx);
            }
            return sL.ToArray();
        }

        public static bool Contains<T>(this T[] tArr, T value)
        {
            foreach (T t in tArr)
            {
                if (t.Equals(value))
                    return true;
                else continue;
            }
            return false;
        }

        //TODO: Create custom Contains function for string.
    }
}
