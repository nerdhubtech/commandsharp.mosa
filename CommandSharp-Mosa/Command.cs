﻿/* This version of CommandSharp was designed for the Mosa Project. And is still subject to the MIT License. For more information please contact WinMister332 at contact@nerdhub.net
 * 
 * CommandSharp for Mosa
 * Written By WinMister332
 * Copyright (c) 2017-2021 NerdHub Technologies, All Rights Reserved.
 * ========================================
 * Class: Command.cs
 * Developer: WinMister332
 * Project: CommandSharp Mosa Edition
 * Date: Friday, September 3rd, 2021 @ 01:03 AM CST
 * License: MIT (https://opensource.org/licenses/MIT)
 *      You must include a copy of the MIT License with
 *      your code if you wish to use this API.
 * Repository: https://www.bitbucket.org/nerdhubtech/commandsharp_mosa/
 * Docs (For Full Edition Only): https://docs.nerdhub.net/CommandSharp/
 * ========================================
 */
using System;

namespace CommandSharp_Mosa
{
    public abstract class Command
    {
        /// <summary>
        /// If true, the syntax of the command will be printed to the console.
        /// </summary>
        public bool ThrowSyntax { get; set; }

        private CommandData commandData;

        public Command(CommandData commandData)
        {
            if (commandData == null)
                throw new NullReferenceException("The parameter: \"commandData\" cannot be null. You MUST provide an instance of the \"commandData\" class.");
            this.commandData = commandData;
        }

        public abstract void OnInvoke(CommandProperties e);

        public CommandData GetData() => commandData;

        public string GetName() => GetData().GetName();
        public string GetDescription() => GetData().GetDescription();
        public string GetAuthor() => GetData().GetAuthor();
        public string GetSyntax() => GetData().GetSyntax();

        public string[] GetAliases() => GetData().GetAliases();
        public string[] GetDependencies() => GetData().GetDependencies();
        public string[] GetSoftDependencies() => GetData().GetSoftDependencies();

        public bool IsHidden() => GetData().IsHidden();
        public bool Hidden
        {
            get => GetData().Hidden;
            set => GetData().Hidden = value;
        }

    }
}