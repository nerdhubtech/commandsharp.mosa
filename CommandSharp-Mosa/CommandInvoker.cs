﻿/* This version of CommandSharp was designed for the Mosa Project. And is still subject to the MIT License. For more information please contact WinMister332 at contact@nerdhub.net
 * 
 * CommandSharp for Mosa
 * Written By WinMister332
 * Copyright (c) 2017-2021 NerdHub Technologies, All Rights Reserved.
 * ========================================
 * Class: CommandInvoker.cs
 * Developer: WinMister332
 * Project: CommandSharp Mosa Edition
 * Date: Friday, September 3rd, 2021 @ 01:03 AM CST
 * License: MIT (https://opensource.org/licenses/MIT)
 *      You must include a copy of the MIT License with
 *      your code if you wish to use this API.
 * Repository: https://www.bitbucket.org/nerdhubtech/commandsharp_mosa/
 * Docs (For Full Edition Only): https://docs.nerdhub.net/CommandSharp/
 * ========================================
 */
using System;
using System.Collections;
using System.Collections.Generic;
namespace CommandSharp_Mosa
{
    public class CommandInvoker
    {
        private bool processQuotes = true;
        private bool ignoreInnerQuotes = false;

        //The list which holds the commands that can be invoked.
        private List<Command> commands;

        public CommandInvoker(bool processQuotes = true, bool ignoreInnerQuotes = false, int maxCommands = -1)
        {
            if (maxCommands < 0)
                commands = new List<Command>();
            else
                commands = new List<Command>(maxCommands);

            this.ignoreInnerQuotes = ignoreInnerQuotes;
            this.processQuotes = processQuotes;

            //Register the embedded commands.
            //RegisterInternalCommands();
        }

        private void RegisterInternalCommands()
        {
            //Register the help command so it's visible.
            Register(new Commands.HelpCommand());
        }

        public void Register(Command command)
        {
            if (!CommandExists(command))
                commands.Add(command); 
        }

        public void Register(Command[] commands)
        {
            foreach (Command cmd in commands)
                Register(cmd);
        }

        public void Unregister(Command command)
        {
            if (CommandExists(command))
                commands.Remove(command);
        }

        public void Unregister(Command[] commands)
        {
            foreach (Command cmd in commands)
                Unregister(cmd);
        }

        public void Override(string oldCommand, Command newCommand)
        {
            Command cmd = GetCommand(oldCommand);
            if (CommandExists(cmd))
                Override(cmd, newCommand);
        }

        public void Override(Command oldCommand, Command newCommand)
        {
            var index = IndexOf(oldCommand);
            if (CommandExists(oldCommand) && !CommandExists(newCommand)) //We don't want a duplicate value of newCommand.
                commands[index] = newCommand; //Inject the newCommand at the index of oldCommand.
        }

        private int IndexOf(Command command)
        {
            for (int i = 0; i < GetCommands().Length; i++)
            {
                Command cmd = commands[i];
                if (CommandExists(command) && (cmd == command))
                    return i;
                else
                    continue;
            }
            return -1;
        }

        private bool CommandExists(Command command)
        {
            foreach (Command cmd in GetCommands())
            {
                if (cmd.Equals(command))
                    return true;
                else continue;
            }
            return false;
        }

        public Command[] GetCommands()
            => commands.ToArray();

        public Command GetCommandByName(string name)
        {
            foreach (Command c in GetCommands())
            {
                if (c.GetName().Equals(name, StringComparison.OrdinalIgnoreCase))
                    return c;
                else continue;
            }
            return null;
        }

        public Command GetCommandByAlias(string alias)
        {
            Command c = null;
            foreach (Command cmd in GetCommands())
            {
                var a = cmd.GetAliases();
                foreach (string s in a)
                {
                    if (s.Equals(alias, StringComparison.OrdinalIgnoreCase))
                    {
                        c = cmd;
                        break;
                    }
                    else continue;
                }
                if (c != null)
                    break;
                else
                    continue;
            }
            return c;
        }

        public Command GetCommand(string value)
        {
            Command cname = GetCommandByName(value);
            Command calias = GetCommandByName(value);
            return cname ?? calias ?? null; //Return the value in 'cname' then 'calias' then null if actually null.
        }

        public void Invoke(string input)
        {
            string name = "";
            string[] args = null;

            if (processQuotes)
            {
                var xargs = ResolveInput(input);
                name = xargs[0];
                //Skip over the name.
                args = Utilities.Skip(xargs, 1);
            }    
            else
            {
                var xargs = Utilities.Split(input, " ");
                name = xargs[0];
                args = Utilities.Skip(xargs, 1);
            }

            Command command = GetCommand(name);

            if (command == null)
                Console.WriteLine("\"" + name + "\", is not a valid internal of external command!");
            else
            {
                CommandProperties properties = new CommandProperties(GetPrompt(), this, new CommandArguments(args), name);
            }
        }

        public void ManualInvoke(Command command, string[] args, CommandProperties cmdProperties = null)
        {
            CommandProperties properties = null;
            if (cmdProperties != null) //Has data.
                properties = cmdProperties;
            else
                properties = new CommandProperties(GetPrompt(), this, new CommandArguments(args), command.GetName());

            //Invoke the command.
            command.OnInvoke(properties);
            if (command.ThrowSyntax)
                Console.WriteLine(properties.PassedName + " " + command.GetSyntax());
        }

        public string[] ResolveInput(string input)
        {
            if (!input.Contains(" "))
                return new string[] { input };
            else if (input.Contains("" ) && !(input.Contains("\"") || input.Contains("\\\"")))
            {
                var xargs = Utilities.Split(input, " ");
                return xargs;
            }
            else
            {
                var xargs = ResolveChars(input);
                return xargs;
            }    
        }

        //Use for processing quotes.
        public string[] ResolveChars(string input)
        {
            bool isInQuotes = false;
            List<string> tokens = new List<string>();
            string s = "";
            if (!ignoreInnerQuotes)
                input = input.Replace("\\\"", "${&quote}");
            for (int i = 0; i < input.Length; i++)
            {
                char c = input[i];
                if (!isInQuotes && (c == ' '))
                {
                    if (s.Length > 0)
                        tokens.Add(s);
                    s = "";
                }
                else
                {
                    if (c == '\"')
                    {
                        if (!isInQuotes)
                            isInQuotes = true;
                        else
                        {
                            if (s.Length > 0)
                            {
                                if (s.StartsWith("\""))
                                    s = s.Remove(0, 1);
                                if (s.EndsWith("\""))
                                    s = s.Remove(s.Length - 1, 1);
                                tokens.Add(s);
                            }
                            s = "";
                            isInQuotes = false;
                        }
                    }
                    else
                    {
                        if (!isInQuotes && c == ' ')
                        {
                            if (s.Length > 0)
                                tokens.Add(s);
                            s = "";
                        }
                        else
                            s += c;
                    }
                }    
            }
            if (!isInQuotes)
            {
                for (int i = 0; i < tokens.Count; i++)
                {
                    if (tokens[i].Contains("${&quote}"))
                        tokens[i] = tokens[i].Replace("${&quote}", "\\\"");
                }
            }
            return tokens.ToArray();
        }

        internal CommandPrompt prompt;

        internal void SetCommandPrompt(CommandPrompt p)
            => prompt = p;

        public CommandPrompt GetPrompt() => prompt;
    }
}